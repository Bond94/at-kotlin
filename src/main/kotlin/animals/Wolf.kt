package animals

import interfaces.Run
import interfaces.Voice

class Wolf(name: String, size: Size) : Carnivorous(name, size), Run, Voice {
    override fun run() {
        println("Бегай волк")
    }

    override fun voice() {
        println("Волк издает звук воя")
    }
}