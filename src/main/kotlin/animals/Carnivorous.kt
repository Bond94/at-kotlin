package animals

import exceptions.WrongFoodException
import food.Food
import food.Grass

abstract class Carnivorous(name: String, size: Size) : Animal(name, size) {
    override fun eat(food: Food) {
        if (food is Grass) {
           throw WrongFoodException()
        }
        println("$name ecт мясо")
    }
}