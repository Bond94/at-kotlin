package animals

import interfaces.Swim

class Fish(name: String, size: Size) : Herbivore(name, size), Swim {
    override fun swim() {
        println("Плавай рыба")
    }
}