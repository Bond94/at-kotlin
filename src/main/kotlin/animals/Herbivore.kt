package animals

import exceptions.WrongFoodException
import food.Food
import food.Meat

abstract class Herbivore(name: String, size: Size) : Animal(name, size) {
    override fun eat(food: Food) {
        if (food is Meat) {
            throw WrongFoodException()
        }
        println("$name ест траву")

    }
}