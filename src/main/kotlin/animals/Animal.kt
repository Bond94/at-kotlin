package animals

import food.Food

abstract class Animal(val name: String, val size: Size) {


    abstract fun eat(food: Food)

}