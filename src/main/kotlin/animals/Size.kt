package animals

enum class Size {
    SMALL,
    MEDIUM,
    HIGH,
    HUGE
}