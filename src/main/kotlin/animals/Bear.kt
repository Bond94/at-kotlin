package animals

import interfaces.Run
import interfaces.Voice

class Bear(name: String, size: Size) : Carnivorous(name, size), Run, Voice {

    override fun run() {
        println("Бегай медведь")
    }

    override fun voice() {
        println("Медведь издает звук рыка")
    }
}