package animals

import interfaces.Fly
import interfaces.Swim
import interfaces.Voice

 class Duck(name: String, size: Size): Herbivore(name, size), Fly, Swim,  Voice {

    override fun fly() {
        println("Летай утка")
    }

    override fun voice() {
        println("Утка говорит Га-га-га")
    }

    override fun swim() {
        println("Плавай утка")
    }


}