package animals

import interfaces.Run
import interfaces.Voice

class Giraffe(name: String, size: Size) : Herbivore(name, size), Run, Voice {
    override fun run() {
        println("Бегай жираф")
    }

    override fun voice() {
        println("Жираф шипит")
    }
}