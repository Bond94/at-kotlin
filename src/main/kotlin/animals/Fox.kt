package animals

import interfaces.Run
import interfaces.Voice

class Fox(name: String, size: Size) : Carnivorous(name, size), Run, Voice {
    override fun run() {
        println("Бегай лиса")
    }

    override fun voice() {
        println("Лиса тявкает")
    }
}