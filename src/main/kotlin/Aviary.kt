import animals.Animal
import animals.Size

class Aviary<in E : Animal>(val size: Size = Size.SMALL) {
    val set: HashSet<Animal> = hashSetOf()

    fun addAnimal(e: E) {
        if (e.size.compareTo(size) <= 0) {
            set.add(e)
            println("${e.name} поместился в размер")
        } else {
            println("Для животного ${e.name} вольер мал")
        }
    }

    fun deleteAnimal(e: E) {
         set.remove(e)
    }

    fun numberAnimals(): Int {
        return set.size
    }


}