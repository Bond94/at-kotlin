package food

abstract class Food {
    abstract fun getFood(): String
}