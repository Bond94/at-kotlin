import animals.*
import food.Grass
import food.Meat
import interfaces.Swim

fun main(args: Array<String>) {
    val bear = Bear("Миша", Size.HIGH)
    val duck = Duck("Гриня", Size.SMALL)
    val fish = Fish("Вася", Size.SMALL)
    val fox = Fox("Алиса", Size.MEDIUM)
    val giraffe = Giraffe("Игорь", Size.HUGE)
    val wolf = Wolf("Серый", Size.HIGH)

    val meat = Meat()
    val grass = Grass()
    val worker = Worker()

    worker.feed(bear, grass)
    worker.feed(bear, meat)
    worker.feed(duck, meat)
    worker.feed(duck, grass)
    worker.feed(wolf, grass)
    worker.feed(wolf, meat)

    worker.getVoice(bear)
    worker.getVoice(giraffe)
    bear.run()
    wolf.run()


    val pond:Array<Swim> = arrayOf(duck, fish)

    println("Количество животных в пруду ${pond.size}")

    for (i in pond.indices) {
        pond[i].swim();
    }

    val herbivore: Aviary<Herbivore> = Aviary(Size.MEDIUM)
    val carnivorous: Aviary<Carnivorous> = Aviary(Size.MEDIUM)

    herbivore.addAnimal(duck)
    carnivorous.addAnimal(bear)
    carnivorous.addAnimal(fox)
    herbivore.addAnimal(fish)
    herbivore.addAnimal(giraffe)

    herbivore.deleteAnimal(fish)

    println("Количество плотоядных животных в вольере " + carnivorous.numberAnimals());
    println("Количество травоядных животных в вольере " + herbivore.numberAnimals());

}