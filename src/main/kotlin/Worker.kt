import animals.Animal
import exceptions.WrongFoodException
import food.Food
import interfaces.Voice

class Worker {
    fun feed(animal: Animal, food: Food) {

        try {
            animal.eat(food)
        } catch (e: WrongFoodException) {
            println("${animal.name} ${food.getFood()}")
        }

    }

    fun getVoice(voice: Voice) {
        voice.voice()
    }
}